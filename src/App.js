import React, { Component } from "react";
import store from "./store";
import { Provider } from "react-redux";
// import { Switch, BrowserRouter as Router } from 'react-router-dom';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/Login";
import User from "./components/User";
import Interest from "./components/interests/Interest";
import Nopage from "./components/Nopage";
import AddInterest from "./components/interests/AddInterest";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Router>
            {/* {this.state.isLoggedin && this.state.isLoggedin == true ? (
              <Header />
            ) : (
              ""
            )} */}
            <Switch>
              <Route path="/" component={Login} exact />
              {/* <Route path="/dashboard" component={dashboard} /> */}
              <Route path="/users" component={User} />
              <Route path="/interests" component={Interest} />
              <Route path="/interest/add" component={AddInterest} />
              <Route component={Nopage} />
            </Switch>
          </Router>
        </div>
      </Provider>
    );
  }
}

export default App;
