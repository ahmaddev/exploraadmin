import { combineReducers } from "redux";
import login from "./loginReducer";
import interest from "./interestReducer";
import users from "./userReducer";

export default combineReducers({ login, interest, users });
