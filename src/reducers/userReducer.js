import {
  GET_USERS_BEGIN,
  GET_USERS_SUCCESS,
  GET_USERS_ERROR
} from "../actions";

const initialState = {
  getUserDataLoading: false,
  usersData: []
};

const users = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS_BEGIN:
      return {
        ...state,
        getUserDataLoading: true
      };
    case GET_USERS_SUCCESS:
      return {
        ...state,
        getUserDataLoading: false,
        usersData: action.data
      };
    case GET_USERS_ERROR:
      return {
        ...state,
        getUserDataLoading: false
      };
    default:
      return state;
  }
};

export default users;
