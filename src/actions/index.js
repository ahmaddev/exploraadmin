export const LOGIN_ADMIN = "LOGIN_ADMIN";
export const LOGIN_BEGIN = "LOGIN_BEGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const INVALID_LOGIN = "INVALID_LOGIN";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const SAVE_INTEREST_BEGIN = "SAVE_INTEREST_BEGIN";
export const SAVE_INTEREST_SUCCESS = "SAVE_INTEREST_SUCCESS";
export const SAVE_INTEREST_ERROR = "SAVE_INTEREST_ERROR";
export const GET_INTEREST_BEGIN = "GET_INTEREST_BEGIN";
export const GET_INTEREST_SUCCESS = "GET_INTEREST_SUCCESS";
export const GET_INTEREST_ERROR = "GET_INTEREST_ERROR";
export const RESET_STATE = "RESET_STATE";
export const GET_USERS_BEGIN = "GET_USERS_BEGIN";
export const GET_USERS_SUCCESS = "GET_USERS_SUCCESS";
export const GET_USERS_ERROR = "GET_USERS_ERROR";

// ========================== login actions ==========================
export const login_begin = () => ({
  type: LOGIN_BEGIN
});
export const login_success = data => ({
  type: LOGIN_SUCCESS,
  data
});
export const invalid_login = data => ({
  type: INVALID_LOGIN,
  data
});
export const login_error = error => ({
  type: LOGIN_ERROR,
  error
});
export const reset_state = () => ({
  type: RESET_STATE
});

export const login_admin = data => {
  return dispatch => {
    dispatch(login_begin());
    console.log("sending data" + data);
    fetch("http://68.183.88.68:3001/ad/adminlogin", {
      body: JSON.stringify(data),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(dat => dat.json())
      .then(dat => {
        console.log("dataaa" + JSON.parse(JSON.stringify(dat)).status);
        var dd = JSON.parse(JSON.stringify(dat));
        console.log("htis is teh status", dd.status);
        if (dd.status == 200) {
          dispatch(login_success(dat));
        } else {
          dispatch(invalid_login(dat));
        }
        return dat;
      })
      .catch(err => {
        console.log("rerorrrrrr", err);
        dispatch(login_error(err));
        console.log("error" + err);
      });
  };
};

//  ================================ save interest's actions ==========================
export const save_interest_begin = () => ({
  type: SAVE_INTEREST_BEGIN
});

export const save_interest_success = data => ({
  type: SAVE_INTEREST_SUCCESS,
  data
});
export const save_interest_error = error => ({
  type: SAVE_INTEREST_ERROR,
  error
});

export const saveInterest = data => {
  return dispatch => {
    dispatch(save_interest_begin());
    console.log("getting data in actions", data);
    fetch("http://68.183.88.68:3001/int/addinterest", {
      body: JSON.stringify(data),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(dat => dat.json())
      .then(dat => {
        console.log("dataaaaaaaa" + JSON.stringify(dat));
        var dd = JSON.stringify(dat);
        dispatch(save_interest_success(dd));
      })
      .catch(err => {
        dispatch(save_interest_error(err));
        console.log("error" + err);
      });
  };
};

//  ========================== get interest actions ==========================
export const get_interest_begin = () => ({
  type: GET_INTEREST_BEGIN
});

export const get_interest_success = data => ({
  type: GET_INTEREST_SUCCESS,
  data
});
export const get_interest_error = error => ({
  type: GET_INTEREST_ERROR,
  error
});

export const getInterest = () => {
  return dispatch => {
    dispatch(get_interest_begin());
    return fetch("http://68.183.88.68:3001/int/gettinginterest")
      .then(data => data.json())
      .then(data => {
        console.log("gg", data.data);
        var dd = data.data;
        dispatch(get_interest_success(dd));
      })
      .catch(err => {
        console.log("error", err);
        dispatch(get_interest_error(err));
      });
  };
};

// ============================ Get users begin ===========================
export const get_users_begin = () => ({
  type: GET_USERS_BEGIN
});
export const get_users_success = data => ({
  type: GET_USERS_SUCCESS,
  data
});

export const get_users_error = error => ({
  type: GET_USERS_ERROR,
  error
});

export const getUsers = () => {
  return dispatch => {
    dispatch(get_users_begin());
    return fetch("http://68.183.88.68:3001/users/getusers")
      .then(data => data.json())
      .then(data => {
        console.log("these are the users ", data.data);
        var dd = data.data;
        dispatch(get_users_success(dd));
      })
      .catch(err => {
        console.log("error", err);
        dispatch(get_users_error(err));
      });
  };
};
