import React, { Component } from "react";

export default class Nopage extends Component {
  render() {
    return (
      <div>
        <h1>Page Not Found</h1>
        <h2>404</h2>
      </div>
    );
  }
}
