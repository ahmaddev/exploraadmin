import React, { Component } from "react";
import xploraimg from "../images/xploraimg.png";
import {
  Container,
  Row,
  Col,
  Button,
  Alert,
  Form,
  FormGroup,
  InputGroup,
  Label,
  InputGroupAddon,
  InputGroupText,
  Input
} from "reactstrap";
import { login_admin } from "../actions";

import { connect } from "react-redux";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }
  handleChange(e) {
    console.log(e);
    this.setState({ [e.target.name]: e.target.value });
  }
  login = event => {
    event.preventDefault();
    var email = this.state.username;
    var password = this.state.password;
    var userjson = {
      email,
      password
    };
    var { dispatch } = this.props;
    dispatch(login_admin(userjson));
  };

  componentDidUpdate() {
    // console.log(this.props.isLoggedIn);
    console.log("This is data------", this.props);
    if (
      this.props.isLoggedIn &&
      this.props.isLoggedIn == true &&
      this.props.status == 200
    ) {
      this.props.history.push("/interests");
    } else {
      // console.log("This is data------", this.props.status);
      // console.log("checking componentwillreceive props");
    }
  }

  render() {
    return (
      <Container className="shiftinglogin">
        <Form onSubmit={this.login}>
          {(this.props.status && this.props.status == 401) ||
          this.props.status == 404 ? (
            <Alert color="danger">Invalid Credentials</Alert>
          ) : (
            ""
          )}
          <Row className="loginform">
            <Col sm="12" md={{ size: 4, offset: 4 }}>
              <img src={xploraimg} className="imagelogin" />

              <FormGroup>
                <Label />
                <Input
                  placeholder="username"
                  name="username"
                  value={this.state.username}
                  onChange={e => this.handleChange(e)}
                />
              </FormGroup>
              <FormGroup>
                <Label />
                <Input
                  placeholder="password"
                  name="password"
                  value={this.state.password}
                  onChange={e => this.handleChange(e)}
                />
              </FormGroup>
              <Label />
              <Button type="submit" color="success">
                Login
                {this.props.loading == true ? (
                  <i className="fa fa-spinner fa-spin" />
                ) : (
                  ""
                )}
              </Button>
            </Col>
          </Row>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.login.isLoggedin,
    loading: state.login.loading,
    loginData: state.login.loginData,
    status: state.login.loginData ? state.login.loginData.status : ""
    // loginError: state.login.loginError
  };
};

export default connect(mapStateToProps)(Login);
