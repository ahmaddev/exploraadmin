import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import xploraimg from "../images/xploraimg.png";
export default class Sidebar extends Component {
  render() {
    // var isActive = this.context.router.route.location.pathname == this.props.to;

    return (
      <div>
        <img src={xploraimg} className="imgSidebar" />
        <ul className="sidebarlist">
          <li>
            <NavLink to="/users" activeClassName="activeli" exact={true}>
              Manage Users
            </NavLink>
          </li>
          <li>
            <NavLink to="/interests" activeClassName="activeli" exact={true}>
              Manage Interest
            </NavLink>
          </li>

          <li>
            <NavLink to="/test" activeClassName="activeli" exact={true}>
              Manage Activities
            </NavLink>
          </li>
        </ul>
      </div>
    );
  }
}
