import React, { Component } from "react";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import { connect } from "react-redux";
import { getUsers } from "../actions/index";
import {
  Container,
  Row,
  Col,
  Button,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  Collapse,
  Table
} from "reactstrap";
const sidebarstyle = {
  height: "690px",
  background: "#eae9e9",
  boxShadow: "3px 1px 3px silver",
  paddingTop: "0px"
};
const imgSidebar = {
  height: "140px",
  width: "108%",
  marginLeft: "15px;"
};
const down = {
  marginTop: "20px"
};

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openNavDropDown: false
    };
  }
  componentDidMount() {
    this.props.dispatch(getUsers());
  }

  toggleDropdown = () => {
    this.setState({
      openNavDropDown: !this.state.openNavDropDown
    });
  };

  render() {
    return (
      <div>
        <Header toggle={this.toggleDropdown} />
        <Row>
          <Col xs="4" sm="2" className="sidebarstyle">
            <Sidebar />
          </Col>
          <Col>
            <Container>
              <h2 style={down} align="left">
                User's
              </h2>
              <Table>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Email</th>
                    <th>Created At</th>
                    <th>Actions</th>
                  </tr>
                </thead>

                {this.props.getUserDataLoading == true ? (
                  <div> Loading... </div>
                ) : (
                  <tbody>
                    {this.props.usersData.length > 0 &&
                      this.props.usersData.map((user, index) => {
                        console.log("these are users", user);
                        return (
                          <tr key={user._id}>
                            <td>{index + 1}</td>
                            <td>{user.firstname}</td>
                            <td>{user.email}</td>
                            <td>{user.createdAt}</td>
                            <td>
                              <i className="fa fa-edit" /> &nbsp;{" "}
                              <i className="fa fa-trash" />
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                )}
              </Table>
            </Container>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getUserDataLoading: state.users.getUserDataLoading,
  usersData: state.users.usersData
});

export default connect(mapStateToProps)(User);
